package org.eclipse.gemoc.addon.utils.timeseries;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.eclipse.gemoc.addon.utils.timeseries.HttpJSONRequestBuilder.HttpJSONRequest;
import org.eclipse.gemoc.addon.utils.timeseries.HttpJSONRequestBuilder.RESTMethod;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle;
import org.eclipse.swt.graphics.Point;

/**
 * Helper class to simplify integer data in function of the current time graphics drawing.
 * It provides simple methods to add new values or draw the graphics in a PNG file you specified.
 * Then, you can get the data-set id, the graph id and the {@link File} object where the graphic was pictured.
 * @see GraphDrawer#addNextValue(int)
 * @see GraphDrawer#drawGraph()
 * @see GraphDrawer#getDataSetId()
 * @see GraphDrawer#getGraphId()
 * @see GraphDrawer#getImage()
 * @see GraphDrawer#getImagePath()
 */
public class GraphDrawer {
	
	/**
	 * The graphics drawing API root address to make REST requests on.
	 */
	public static String ROOT_ADDRESS = "http://localhost:4000";
	

	/**
	 * The REST request builder used to build requests.
	 */
	private HttpJSONRequestBuilder builder;

	/**
	 * The path of the file where to draw the graphic.
	 */
	private String path;

	/**
	 * The PNG file of the graphic.
	 */
	private File image;

	/**
	 * The data-set id.
	 */
	private String dataSetId;

	/**
	 * The graph id.
	 */
	private String graphId;

	/**
	 * The graph points.
	 */
	private List<Point> points;

	/**
	 * The number of displayed points at the same time.
	 */
	private int nbValues;

	/**
	 * The drawn line style.
	 * @see GraphDrawer#LineStyle
	 */
	private LineStyle style;

	/**
	 * GraphDrawer constructor. It initializes the request builder with the root address and the image path.
	 * Then, initialize a data-set for the future data and a graph for the future display and link the data-set to the graph.
	 * @param graphPath : the path of the file where to draw the graphic.
	 * @param graphTitle : the name the graphic should show.
	 * @see GraphDrawer#initDataSet()
	 * @see GraphDrawer#initGraph(String, String, String)
	 */
	public GraphDrawer(String graphPath, String xLabel, String yLabel, String graphTitle, int nbValues, LineStyle style) {
		this.builder = new HttpJSONRequestBuilder(ROOT_ADDRESS);
		this.path = graphPath;
		this.points = new ArrayList<>();
		this.nbValues = nbValues;
		this.style = style;
		initDataSet();
		initGraph(xLabel, yLabel, graphTitle);
		HttpJSONRequest link = builder.createRequest("/graphs/"+graphId+"/dataSet/"+dataSetId, RESTMethod.POST);
		link.getResponse();
		link.disconnect();
	}

	/**
	 * Initialize the data-set id by creating a new one.
	 * @see GraphDrawer#dataSetId
	 */
	private void initDataSet() {
		HttpJSONRequest dataSet = builder.createRequest("/dataSets/", RESTMethod.POST);
		JSONParameters params = new JSONParameters()
				.addParameter("name", "dataset")
				.addParameter("description", "a dataset")
				.addParameter("points", formatedPoints());
		dataSetId = dataSet.getResponse(params).replaceAll("\"", "");
		dataSet.disconnect();
	}

	/**
	 * Initialize the graph id by creating a new one with a given name.
	 * @param name : the name the graphic should show.
	 * @see GraphDrawer#graphId
	 */
	private void initGraph(String xLabel, String yLabel, String title) {
		HttpJSONRequest graph = builder.createRequest("/graphs", RESTMethod.POST);
		//JSONParameters bounds = new JSONParameters().addParameter("lowerBound", 0).addParameter("upperBound", nbValues);
		JSONParameters param = new JSONParameters()
				.addParameter("name", title)
				.addParameter("type", "connectedLine")
				.addParameter("showGrid", false)
				.addParameter("showX", true)
				.addParameter("showY", true)
		//		.addParameter("xBounds", bounds)
				.addParameter("graphLegend", title)
				.addParameter("xLegend", xLabel)
				.addParameter("yLegend", yLabel);
		graphId = graph.getResponse(param);
		graph.disconnect();
	}

	/**
	 * Adding a value to the graphic. That value will be associated with the current time.
	 * @param x : the next X integer value to add to the data-set.
	 * @param y : the next Y integer value to add to the data-set.
	 * @return itself.
	 */
	public boolean addNextValue(int x, int y) {
		boolean pointAdded = false;
		if (points.isEmpty()) {
			points.add(new Point(x, y));
			pointAdded = true;
		}else {
			Point lastPoint = points.get(points.size()-1);
			if (x != lastPoint.x || y != lastPoint.y) {
				if(style.equals(LineStyle.DIGITAL) ) { //to have instantaneous change and not sloppy ones
					points.add(new Point(x, lastPoint.y));
				}
				points.add(new Point(x, y));
				pointAdded = true;
			}
		}
		if(pointAdded) {
			HttpJSONRequest valueRequest = builder.createRequest("/dataSets/"+dataSetId, RESTMethod.PUT);
			JSONParameters param = new JSONParameters().addParameter("points", formatedPoints());
			valueRequest.getResponse(param);
			valueRequest.disconnect();
		}
		return pointAdded;
	}

	/**
	 * Draw the current data-set on the graphic and write the picture in the PNG file of the set path.
	 * @return itself.
	 * @see GraphDrawer#path
	 * @see GraphDrawer#image
	 */
	public GraphDrawer drawGraph() throws IOException {
		HttpJSONRequest imageRequest = builder.createRequest("/graphs/"+graphId+"/render?type=PNG", RESTMethod.GET);
		String[] imageRes = imageRequest.getResponse().split(",");
		if(imageRes.length > 1) {
			String base64Image = imageRes[1].split("\"")[0];
			ByteArrayInputStream imageStream = new ByteArrayInputStream(java.util.Base64.getDecoder().decode(base64Image));
			BufferedImage bufferedImage = ImageIO.read(imageStream);
			
			imageStream.close();
			image = new File(path);
			ImageIO.write(bufferedImage, "png", image);
		}
		return this;
	}
	
	private List<JSONParameters> formatedPoints() {
		List<JSONParameters> formated = new ArrayList<>();
		int maxSize = Math.min(nbValues, points.size());
		for(int i = 0; i < maxSize; i++) {
			Point point = points.get(i+points.size()-maxSize);
			formated.add(new JSONParameters().addParameter("x", point.x).addParameter("y", point.y));
		}
		return formated;
	}

	/**
	 * @return the data-set id.
	 */
	public String getDataSetId() {
		return dataSetId;
	}

	/**
	 * @return the graph id.
	 */
	public String getGraphId() {
		return graphId;
	}

	/**
	 * @return the image file where the graphic should be written.
	 * @see GraphDrawer#drawGraph()
	 */
	public File getImage() {
		return image;
	}

	/**
	 * @return the path of the image file where the graphic should be written.
	 * @see GraphDrawer#drawGraph()
	 */
	public String getImagePath() {
		return path;
	}
}
