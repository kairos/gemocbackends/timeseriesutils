package org.eclipse.gemoc.addon.utils.timeseries;

import java.util.List;

/**
 * This class provides a JSON format parameters builder.
 */
public class JSONParameters {

	/**
	 * The JSON {@link String} format current parameters.
	 */
	private String parameters = "";

	/**
	 * Add a parameter to the parameter list (as a {@link String}).
	 * @param name : the name of the parameter.
	 * @param value : the value of the parameter.
	 * @see JSONParameters#parameters
	 */
	private void setParameter(String name, String value) {
		if(!parameters.equals("")) parameters += ", ";
		parameters += "\""+name+"\": "+value; // Careful, the name must be saw as a string into the JSON parameter list.
	}

	/**
	 * Add a {@link Integer} parameter to the parameter list (as a {@link String}).
	 * @param name : the name of the parameter.
	 * @param value : the value of the parameter.
	 * @see JSONParameters#setParameter(String, String)
	 */
	public JSONParameters addParameter(String name, int value) {
		setParameter(name, ""+value);
		return this;
	}

	/**
	 * Add a {@link Long} parameter to the parameter list (as a {@link String}).
	 * @param name : the name of the parameter.
	 * @param value : the value of the parameter.
	 * @see JSONParameters#setParameter(String, String)
	 */
	public JSONParameters addParameter(String name, long value) {
		setParameter(name, ""+value);
		return this;
	}

	/**
	 * Add a {@link Boolean} parameter to the parameter list (as a {@link String}).
	 * @param name : the name of the parameter.
	 * @param value : the value of the parameter.
	 * @see JSONParameters#setParameter(String, String)
	 */
	public JSONParameters addParameter(String name, boolean value) {
		setParameter(name, ""+value);
		return this;
	}

	/**
	 * Add a {@link String} parameter to the parameter list (as a {@link String}).
	 * @param name : the name of the parameter.
	 * @param value : the value of the parameter.
	 * @see JSONParameters#setParameter(String, String)
	 */
	public JSONParameters addParameter(String name, String value) {
		setParameter(name, "\""+value+"\""); // Careful, the value must be saw as a string into the JSON parameter list.
		return this;
	}

	/**
	 * Add a {@link List} of {@link JSONParameters} parameter to the parameter list (as a {@link List} of {@link String}).
	 * @param name : the name of the parameter.
	 * @param value : the value of the parameter.
	 * @see JSONParameters#setParameter(String, String)
	 */
	public JSONParameters addParameter(String name, List<JSONParameters> value) {
		setParameter(name, value.toString());
		return this;
	}

	/**
	 * Add a {@link JSONParameters} parameter to the parameter list (as a {@link String}).
	 * @param name : the name of the parameter.
	 * @param value : the value of the parameter.
	 * @see JSONParameters#setParameter(String, String)
	 */
	public JSONParameters addParameter(String name, JSONParameters value) {
		setParameter(name, value.toString());
		return this;
	}
	
	/**
	 * @return the JSON formated parameters.
	 */
	@Override
	public String toString() {
		return "{"+parameters+"}";
	}
	
}
