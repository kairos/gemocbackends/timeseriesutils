package org.eclipse.gemoc.addon.utils.timeseries;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

/**
 * This class provides a builder of {@link HttpJSONRequest} to create simple HTTP requests using the JSON format.
 */
public class HttpJSONRequestBuilder {
	
	/**
	 * Root address from which all built {@link HttpJSONRequest} should start.
	 */
	private final String rootAddr;

	/**
	 * HttpJSONRequestBuilder constructor. It initializes the root address.
	 * @param rootAddr : the root address for {@link HttpJSONRequest} to be built on.
	 * @see HttpJSONRequestBuilder#rootAddr
	 */
	public HttpJSONRequestBuilder(String rootAddr) {
		this.rootAddr = rootAddr;
	}

	/**
	 * @param path : the address of the request without taking the root address into account.
	 * @param method : the {@link RESTMethod} to be used for the request.
	 * @return a {@link HttpJSONRequest} built on the {@link HttpJSONRequestBuilder} root address.
	 */
	public HttpJSONRequest createRequest(String path, RESTMethod method) {
		try {
			return new HttpJSONRequest(rootAddr+path, method);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * This class provides an HTTP request with simple methods allowing to:<br>
	 * -  get the response of the request,<br>
	 * -  get the status and message of the response of the request,<br>
	 * -  get the headers of the response of the request,<br>
	 * -  disconnect the request when done with it.
	 * @see HttpJSONRequest#getResponse()
	 * @see HttpJSONRequest#getResponse(JSONParameters)
	 * @see HttpJSONRequest#getStatusAndMessage()
	 * @see HttpJSONRequest#getHeaders()
	 * @see HttpJSONRequest#disconnect()
	 */
	public class HttpJSONRequest {

		/**
		 * The connection object we work with to make the HTTP request.
		 */
		private HttpURLConnection connexion;

		/**
		 * HttpJSONRequest constructor. It initializes a HTTP request ({@link HttpURLConnection}) for the right link and method.
		 * @param link : the request link.
		 * @param method : the {@link RESTMethod} to be used for the request.
		 * @see HttpJSONRequest#connexion
		 */
		private HttpJSONRequest(String link, RESTMethod method) throws IOException {
			connexion = (HttpURLConnection) new URL(link).openConnection();
			connexion.setRequestMethod(method.name());
			connexion.setRequestProperty("Content-Type", "application/json");
		}

		/**
		 * @return the status and message of the response of the request.
		 */
		public String getStatusAndMessage() {
			try {
				return connexion.getResponseCode()+" "+connexion.getResponseMessage();
			} catch (IOException e) {
				return e.getMessage();
			}
		}

		/**
		 * @return the response of the request for no parameters.
		 */
		public String getResponse() {
			return getResponse(null);
		}

		/**
		 * @return the response of the request for the given parameters.
		 */
		public String getResponse(JSONParameters parameters) {
			StringBuilder response = new StringBuilder();
			try {
				if(parameters != null) initParameters(parameters); // add the parameters them to the request if not null.
				int status = connexion.getResponseCode();
				// Simple way to know if a response should be considered as an error.
				InputStream input = (status>299)?connexion.getErrorStream():connexion.getInputStream();
				Reader responseReader = new java.io.InputStreamReader(input);
				BufferedReader in = new BufferedReader(responseReader);
				String line;
				while((line = in.readLine()) != null) {
					response.append(line);
				}
				in.close();
			} catch (IOException e) {
				System.err.println("HTTP request failed");
			}
			return response.toString();
		}

		/**
		 * Set the current parameters to the request.
		 */
		private void initParameters(JSONParameters parameters) throws IOException {
			connexion.setDoOutput(true);
			DataOutputStream out = new DataOutputStream(connexion.getOutputStream());
			out.writeBytes(parameters.toString());
			out.flush();
			out.close();
		}

		/**
		 * @return the headers of the response of the request.
		 */
		public String getHeaders() {
			StringBuilder headers = new StringBuilder();
			connexion.getHeaderFields().entrySet().stream().filter(e -> e.getKey() != null).forEach(entry -> {
				headers.append(entry.getKey()).append(": ");
				Iterator<String> it = entry.getValue().iterator();
				if(it.hasNext()) {
					headers.append(it.next());
					while(it.hasNext()) {
						headers.append(", ").append(it.next());
					}
				}
				headers.append("\n");
			});
			String res = headers.toString();
			return (res.length()>0)?res.substring(0, res.length()-1):res;
		}

		/**
		 * Disconnect the request.
		 */
		public void disconnect() {
			connexion.disconnect();
		}

		/**
		 * @return the status and message of the response of the request.
		 */
		@Override
		public String toString() {
			return getStatusAndMessage(); // + "\n" + getHeaders();
		}
		
	}

	/**
	 * Enumeration of {@link HttpJSONRequestBuilder} handled REST methods.
	 */
	public enum RESTMethod {
		GET, POST, PUT;
	}
	
}
